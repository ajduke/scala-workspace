object Minimum{
	def main(args:Array[String]){
		for( argument <- args) {
			var st = endWithE( argument)
			println(argument+" : "+st)
		}

	}

	def endWithE(str: String) =str last match {
		case 'e' => true
		case 'E' => true
		case _ => false
	}

}

