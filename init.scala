 def init[T](xs: List[T]):List[T] = xs match {
     case List()=> throw new Error("init of empty list")
      case List(x) => Nil
      case y::ys => y :: init(ys)
}


def reverse[T](xs: List[T]):List[T] = xs match {
     case List()  => Nil
     case y :: ys => reverse(ys) ++ List(y)
}

def removeAt[T](n:Int, xs:List[T]){
	(xs take n) ::: (xs drop n+1)

}